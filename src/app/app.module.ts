import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { StaffRoutingModule } from './portal/staffArea/staff-routing-module.module';
import { StudentRoutingModule } from './portal/studentArea/student-routing-module.module';
import { RouterModule, Routes } from '@angular/router';
import { NotificationService } from '../services/notification.service';
import { UserService } from '../services/user.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PortalModule } from './portal/portal.module';
import { ValidationErrorService } from '../services/validation-error.service';
import { EventsService } from '../services/event.service';
import { AuthenticationService } from '../services/authentication.service';
import { ApiHandlerService } from '../services/api-handler.service';
import { NotificationComponent } from '../shared/components/notification/notification.component';
import { ScriptLoaderService } from '../services/script-loader.service';
import { StudentLoginComponent } from './portal/studentArea/student-authentication/student-login.component';
import { HeaderComponentComponent } from './portal/staffArea/staff-dashboard/layoutComponents/header-component/header-component.component';
import { SideBarComponentComponent } from './portal/staffArea/staff-dashboard/layoutComponents/side-bar-component/side-bar-component.component';
import { SchoolService } from '../services/school.service';

const APP_ROUTES: Routes = [
  { path: "", component: StudentLoginComponent },
  { path: "portal", loadChildren: './portal/portal.module#PortalModule' }
]


@NgModule({
  declarations: [
    AppComponent,
    NotificationComponent,
    StudentLoginComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule.forRoot(APP_ROUTES)
  ],
  providers: [
    ApiHandlerService,
    NotificationService,
    UserService,
    ValidationErrorService,
    EventsService,
    AuthenticationService,
    SchoolService,
    ScriptLoaderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
