import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { StaffDashboardModule } from "./staff-dashboard/dashboard.module";
import { StaffLoginComponent } from "./staff-authentication/staff-login.component";
import { StaffDashboardComponent } from "./staff-dashboard/dashboard-component.component";
import { StudentsComponent } from "./staff-dashboard/students/students.component";
import { ResultsComponent } from "./staff-dashboard/results/results.component";
import { ReportsComponent } from "./staff-dashboard/reports/reports.component";
import { HostelsComponent } from "./staff-dashboard/hostels/hostels.component";
import { FeesComponent } from "./staff-dashboard/fees/fees.component";
import { FacultiesComponent } from "./staff-dashboard/faculties/faculties.component";
import { DepartmentsComponent } from "./staff-dashboard/departments/departments.component";
import { CoursesComponent } from "./staff-dashboard/courses/courses.component";
import { ApplicantsComponent } from "./staff-dashboard/applicants/applicants.component";
import { StaffComponent } from "./staff-dashboard/staff/staff.component";
import { ResultsConfigComponent } from "./staff-dashboard/configurations/results/results.component";
import { StaffConfigComponent } from "./staff-dashboard/configurations/staff/staff.component";
import { StudentsConfigComponent } from "./staff-dashboard/configurations/students/students.component";
import { ReportsConfigComponent } from "./staff-dashboard/configurations/reports/reports.component";
import { FeesConfigComponent } from "./staff-dashboard/configurations/fees/fees.component";
import { FacultiesConfigComponent } from "./staff-dashboard/configurations/faculties/faculties.component";
import { DepartmentsConfigComponent } from "./staff-dashboard/configurations/departments/departments.component";
import { CoursesConfigComponent } from "./staff-dashboard/configurations/courses/courses.component";
import { ApplicantsConfigComponent } from "./staff-dashboard/configurations/applicants/applicants.component";
import { HostelsConfigComponent } from "./staff-dashboard/configurations/hostels/hostels.component";
import { StaffConfigurationsComponent } from "./staff-dashboard/configurations/configurations.component";
import { HeaderComponentComponent } from "./staff-dashboard/layoutComponents/header-component/header-component.component";
import { SideBarComponentComponent } from "./staff-dashboard/layoutComponents/side-bar-component/side-bar-component.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";


const STAFF_ROUTES: Routes = [

    { path: "", component: StaffLoginComponent },
    {
        path: 'office', component: StaffDashboardComponent,
        children: [
            { path: 'students', component: StudentsComponent },
            { path: 'results', component: ResultsComponent },
            { path: 'reports', component: ReportsComponent },
            { path: 'staff', component: StaffComponent },
            { path: 'hostels', component: HostelsComponent },
            { path: 'fees', component: FeesComponent },
            { path: 'faculties', component: FacultiesComponent },
            { path: 'departments', component: DepartmentsComponent },
            { path: 'courses', component: CoursesComponent },
            { path: 'applicants', component: ApplicantsComponent },
            {
                path: 'config',children: [
                    { path: 'results', component: ResultsConfigComponent },
                    { path: 'staff', component: StaffConfigComponent },
                    { path: 'students', component: StudentsConfigComponent },
                    { path: 'reports', component: ReportsConfigComponent },
                    { path: 'fees', component: FeesConfigComponent },
                    { path: 'faculties', component: FacultiesConfigComponent },
                    { path: 'departments', component: DepartmentsConfigComponent },
                    { path: 'courses', component: CoursesConfigComponent },
                    { path: 'applicants', component: ApplicantsConfigComponent },
                    { path: 'hostels', component: HostelsConfigComponent }
                ]
            }
        ]
    },

]

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(STAFF_ROUTES)
    ],
    exports: [
        RouterModule,
        StaffLoginComponent,
        HeaderComponentComponent,
        SideBarComponentComponent
    ],
    declarations: [
        HeaderComponentComponent,
        SideBarComponentComponent,
        StaffLoginComponent,
        ApplicantsComponent,
        CoursesComponent,
        DepartmentsComponent,
        FacultiesComponent,
        FeesComponent,
        HostelsComponent,
        ReportsComponent,
        ResultsComponent,
        StudentsComponent,
        StaffDashboardComponent,
        StaffComponent,
        ResultsConfigComponent,
        StaffConfigComponent,
        StudentsConfigComponent,
        ReportsConfigComponent,
        StaffConfigurationsComponent,
        FeesConfigComponent,
        FacultiesConfigComponent,
        DepartmentsConfigComponent,
        CoursesConfigComponent,
        ApplicantsConfigComponent,
        HostelsConfigComponent
    ]
})


export class StaffRoutingModule { }