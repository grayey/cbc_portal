
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import * as utils from '../../../../../utils/util';
import { Faculty } from '../../../../../interfaces/faculty.inteface';
import { Department } from '../../../../../interfaces/department.interface';
import { Course } from '../../../../../interfaces/course.interface';
import { ScriptLoaderService } from '../../../../../services/script-loader.service';
import { SchoolService } from '../../../../../services/school.service';
import { UserService } from '../../../../../services/user.service';
import { NotificationService } from '../../../../../services/notification.service';


declare const $: any;
@Component({
    selector: 'app-staff-component',
    templateUrl: './staff.component.html',
    styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit {

    public overlay = utils.overlay;
    public allStaffs: any[] = [];
    public updatedStaff: any;
    public allGroups: any[] = [];
    public allDepartments: Department[] = [];
    public allCourses: Course[] = [];
    public allStates: any[] = [];
    public allCountries: any[] = [];
    public authenticatedUser: any;
    public myStaffs: any[] = [];
    public myEditedStaff: any = {};
    public createStaffForm: FormGroup;
    public updateStaffForm: FormGroup;
    public editedIndex: number;
    public load = {
        requesting: {
            list: true,
            create: false
        },
        message: {
            create: "Create",
            update: "Update"
        },
        departments: {
            list: false,
            loading: false
        },
        courses: {
            list: false,
            loading: false
        },
        Staffs: {
            list: false,
            loading: false
        }

    }




    constructor(
        private _script: ScriptLoaderService,
        private userService: UserService,
        private schoolService: SchoolService,
        private fb: FormBuilder,
        private Alert: NotificationService) {
        // this.authenticatedUser = this.userService.getAuthUser().login.user;
        this.createStaffForm = this.fb.group(StaffComponent.createStaffForm());
        this.updateStaffForm = this.fb.group(StaffComponent.updateStaffForm());
        console.log("Authenticated user ", this.authenticatedUser);
    }
    ngOnInit() {
        this.logStaffIn();


    }


    // ngAfterViewInit() {
    //     this._script.loadScripts('app-staff-component',
    //         ['assets/demo/default/custom/components/datatables/base/html-table.js']);

    // }


    static createStaffForm = function () {
        return {
            email: ['', Validators.compose([Validators.required])],
            fullname: ['', Validators.compose([Validators.required])],
            group_id: '',
            employee_id: ['', Validators.compose([Validators.required])],
            active_hour_id: ['', Validators.compose([Validators.required])]
            // department_id: ['', Validators.compose([Validators.required])],
            // course_of_study_id: ['', Validators.compose([Validators.required])],
            // country_id: ['', Validators.compose([Validators.required])],
            // state_id: '',
            // matric_no: ['', Validators.compose([Validators.required])]
            // {"fullname":["The fullname field is required."],"group_id":["The group id field is required."],"employee_id":["The employee id field is required."],"active_hour_id":["The active hour id field is required."]}
        }
        // email,first_name,last_name,course_of_study_id
    }



    static updateStaffForm = function () {
        return {

            email: ['', Validators.compose([Validators.required])],
            fullname: ['', Validators.compose([Validators.required])],
            group_id: '',
            employee_id: ['', Validators.compose([Validators.required])],
            active_hour_id: ['', Validators.compose([Validators.required])],

            // department_id: ['', Validators.compose([Validators.required])],
            // course_of_study_id: ['', Validators.compose([Validators.required])],
            // country_id: ['', Validators.compose([Validators.required])],
            // state_id: '',
            // matric_no: ['', Validators.compose([Validators.required])]
        }
    }


    private getAllStaffs() {
        this.load.requesting.list = true;
        this.schoolService.getAllStaffs().subscribe(
            (allStaffsResponse) => {
                this.load.requesting.list = false;
                this.allStaffs = allStaffsResponse.data;
                console.log("All Staffs response", allStaffsResponse)
            },
            (error) => {
                this.Alert.error("An error occurred, could not load staff", error);
                this.load.requesting.list = false;
            }
        )
    }

    private async createStaff() {

        this.load.requesting.create = true;
        this.load.message.create = "Creating...";

        console.log("Create Staff object ", this.createStaffForm.value);
        await this.schoolService.createOrUpdateStaff(this.createStaffForm.value).subscribe(
            (createdStaffResponse) => {
                this.load.message.create = "Create";
                this.Alert.success(`${this.createStaffForm.value.name} created successfully\n`);
                this.createStaffForm = this.fb.group(StaffComponent.createStaffForm());
                this.load.requesting.create = false;
                this.triggerModalOrOverlay('close', 'createStaff');
                this.myStaffs.push(createdStaffResponse);
                console.log("Newly created school ", createdStaffResponse)
            },
            (error) => {
                this.load.message.create = "Create";
                // this.triggerModal('close','createSchool'); 
                console.log("Eroorroroor ", error);
                this.load.requesting.create = false;
                this.Alert.error(`Could not create ${this.createStaffForm.value.name}`, error)
            }
        )
    }

    private updateStaff() {
        this.load.requesting.create = true;
        this.load.message.update = "Updating...";

        this.schoolService.createOrUpdateStaff(this.updateStaffForm.value, this.updatedStaff.id).subscribe(
            (updatedStaffResponse) => {
                this.load.message.update = "Update";
                this.updateStaffForm = this.fb.group(StaffComponent.updateStaffForm());
                this.Alert.success(`${this.updateStaffForm.value.name} updated successfully\n`);
                this.allStaffs[this.editedIndex] = updatedStaffResponse;
                this.load.requesting.create = false;
                this.triggerModalOrOverlay('close', 'updateStaff')

                console.log("Updated school ", this.updateStaffForm)
            },
            (error) => {
                this.load.message.update = "Update";
                this.load.requesting.create = false;
                this.Alert.error(`Could not update ${this.updateStaffForm.value.first_name}`, error)
            }
        )
    }


    private getAllGroups() {
        this.schoolService.getAllGroups().subscribe(
            (groupsResponse) => {
                console.log("All groups ", groupsResponse);
                this.allGroups = groupsResponse.data;
                this.updatedStaff.loading = false;
            },
            (error) => {
                this.Alert.error('Sorry, could not load faculties. Please reload page.', error);
                this.updatedStaff.loading = false;
            }
        )
    }

    /**
     * This method returns a single faculty which contains a list of departments
     */
    private getDepartmentByByFacultyId(facultyId) {
        this.load.departments.list = true;
        this.load.departments.loading = true;

        this.schoolService.getDepartmentByFacultyId(facultyId).subscribe(
            (departmentResponse) => {
                this.load.departments.loading = false;

                this.allDepartments = departmentResponse.departments;
                console.log("returned departments", departmentResponse);
            },
            (error) => {
                this.load.departments.list = false;
                this.load.departments.loading = false;

                this.Alert.error(`Sorry could not load departments`, error)
            }

        )
    }

    public async getAllCourses() {
        await this.schoolService.getAllCourses().subscribe(
            (allCoursesResponse) => {
                this.allCourses = allCoursesResponse.data;
                this.updatedStaff.loading = false;
            },
            (error) => {
                this.Alert.error("Sorry, could not load school courses", error);
                this.updatedStaff.loading = false;

            })
    }

    public getCourseByDepartmentId(departmentId) {
        this.load.courses.list = true;
        this.load.courses.loading = true;
        this.schoolService.getCourseByDepartmentId(departmentId).subscribe(

            (coursesResponse) => {
                this.load.courses.loading = false;

                this.allCourses = coursesResponse.course_of_studies;
                console.log("returned courses", coursesResponse);
            },
            (error) => {
                this.load.courses.list = false;
                this.load.courses.loading = false;
                this.Alert.error(`Sorry could not load courses`, error)
            }

        )
    }

    public getAllCountries() {
        this.schoolService.getAllCountries().subscribe(
            (countriesResponse) => {
                this.allCountries = countriesResponse;
                console.log("returned countries", countriesResponse);
            },
            (error) => {
                this.Alert.error(`Sorry could not load countries`, error)
            }

        )
    }

    public getStateByCountryId(countryId) {
        this.schoolService.getStateByCountryId(countryId).subscribe(
            (statesResponse) => {
                this.allStates = statesResponse.states;
                console.log("returned States", statesResponse);
            },
            (error) => {
                this.Alert.error(`Sorry could not load States`, error)
            }

        )
    }


    public logStaffIn() {
        this.schoolService.logStaffIn().subscribe(
            (loginResponse) => {
                this.userService.setAuthUser(loginResponse.token);
                this.getAllStaffs();
            }
        )
    }

    public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
        if (ind >= 0) {
            this.allStaffs[ind].loading = true;
            this.updatedStaff = this.allStaffs[ind];
            this.getAllGroups();
            const StaffManagementObject = {
                fullname: this.updatedStaff.fullname,
                email: this.updatedStaff.email,
                employee_id: this.updatedStaff.employee_id,
                active_hour_id: this.updatedStaff.active_hour.id,
                group_id: this.updatedStaff.group.id

            }
            this.updateStaffForm.patchValue(StaffManagementObject);
            console.log("Staff management object ", StaffManagementObject);

            // this.updatedStaff.loading=false    
        }
        (action === "open") ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');

        // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
        // });

    }


}
