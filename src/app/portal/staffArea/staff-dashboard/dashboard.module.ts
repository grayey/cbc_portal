import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StaffDashboardComponent } from './dashboard-component.component';
import { StaffComponent } from './staff/staff.component';
import { StudentsComponent } from './students/students.component';
import { ResultsComponent } from './results/results.component';
import { ReportsComponent } from './reports/reports.component';
import { HostelsComponent } from './hostels/hostels.component';
import { FeesComponent } from './fees/fees.component';
import { FacultiesComponent } from './faculties/faculties.component';
import { DepartmentsComponent } from './departments/departments.component';
import { CoursesComponent } from './courses/courses.component';
import { ApplicantsComponent } from './applicants/applicants.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';



const STAFF_DASHBOARD_ROUTES: Routes = [
    {
        path: '', component: StaffDashboardComponent, pathMatch: "full",
        children: [
            { path: 'students', component: StudentsComponent },
            { path: 'results', component: ResultsComponent },
            { path: 'reports', component: ReportsComponent },
            { path: 'hostels', component: HostelsComponent },
            { path: 'fees', component: FeesComponent },
            { path: 'faculties', component: FacultiesComponent },
            { path: 'departments', component: DepartmentsComponent },
            { path: 'courses', component: CoursesComponent },
            { path: 'applicants', component: ApplicantsComponent },
            // {path:"config", loadChildren:"./configurations/configurations.module#ConfigurationsModule"}
        ]
    },

   




]

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(STAFF_DASHBOARD_ROUTES)
    ],
    exports: [
        ApplicantsComponent,
        CoursesComponent,
        DepartmentsComponent,
        FacultiesComponent,
        FeesComponent,
        HostelsComponent,
        ReportsComponent,
        ResultsComponent,
        StudentsComponent,
        StaffComponent,
        StaffDashboardComponent,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule
    ],
    declarations: [
        ApplicantsComponent,
        CoursesComponent,
        DepartmentsComponent,
        FacultiesComponent,
        FeesComponent,
        HostelsComponent,
        ReportsComponent,
        ResultsComponent,
        StudentsComponent,
        StaffDashboardComponent,
        StaffComponent
    ]
})
export class StaffDashboardModule { }
