import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentsConfigComponent } from './students/students.component';
import { ResultsConfigComponent } from './results/results.component';
import { StaffConfigComponent } from './staff/staff.component';
import { ReportsConfigComponent } from './reports/reports.component';
import { HostelsConfigComponent } from './hostels/hostels.component';
import { FeesConfigComponent } from './fees/fees.component';
import { FacultiesConfigComponent } from './faculties/faculties.component';
import { DepartmentsConfigComponent } from './departments/departments.component';
import { CoursesConfigComponent } from './courses/courses.component';
import { ApplicantsConfigComponent } from './applicants/applicants.component';
import { RouterModule, Routes } from '@angular/router';
import { StaffConfigurationsComponent } from './configurations.component';


const CONFIG_ROUTES: Routes = [
    {
        path: "", component: StaffConfigurationsComponent, children: [
            { path: 'results', component: ResultsConfigComponent },
            { path: 'staff', component: StaffConfigComponent },
            { path: 'students', component: StudentsConfigComponent },
            { path: 'reports', component: ReportsConfigComponent },
            { path: 'fees', component: FeesConfigComponent },
            { path: 'faculties', component: FacultiesConfigComponent },
            { path: 'departments', component: DepartmentsConfigComponent },
            { path: 'courses', component: CoursesConfigComponent },
            { path: 'applicants', component: ApplicantsConfigComponent },
            { path: 'hostels', component: HostelsConfigComponent }
        ]
    }



]

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(CONFIG_ROUTES)
    ],
    declarations: [
        ApplicantsConfigComponent,
        CoursesConfigComponent,
        DepartmentsConfigComponent,
        FacultiesConfigComponent,
        FeesConfigComponent,
        HostelsConfigComponent,
        ReportsConfigComponent,
        StaffConfigComponent,
        ResultsConfigComponent,
        StudentsConfigComponent,
        StaffConfigurationsComponent
    ],
    exports: [
        ApplicantsConfigComponent,
        CoursesConfigComponent,
        DepartmentsConfigComponent,
        FacultiesConfigComponent,
        FeesConfigComponent,
        HostelsConfigComponent,
        ReportsConfigComponent,
        StaffConfigComponent,
        ResultsConfigComponent,
        StudentsConfigComponent,
        StaffConfigurationsComponent,
        RouterModule
    ]
})
export class ConfigurationsModule { }
