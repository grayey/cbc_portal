import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../../../services/script-loader.service';

@Component({
    selector: 'app-dashboard-component',
    templateUrl: './dashboard-component.component.html',
    styleUrls: ['./dashboard-component.component.css']
})

export class StaffDashboardComponent implements OnInit,AfterViewInit {

    constructor(
        private _script: ScriptLoaderService,        
    ) { }

    ngOnInit() {
        // alert("Portal student Component!!");
    }

    ngAfterViewInit() {
        this._script.loadScripts('app-dashboard-component',
            [ "./assets/student_assets/app/js/dashboard.js","./assets/student_assets/demo/demo4/base/scripts.bundle.js"],true);
  
    }
}
