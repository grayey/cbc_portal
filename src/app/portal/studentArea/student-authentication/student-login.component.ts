import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../../services/user.service';
import { AuthenticationService } from '../../../../services/authentication.service';
import { NotificationService } from '../../../../services/notification.service';

@Component({
    selector: 'app-student-login',
    templateUrl: './student-login.component.html',
    styleUrls: ['./student-login.component.css']
})
export class StudentLoginComponent implements OnInit {

    title = 'app';
    model: any = {};
    loading = false;
    returnUrl: string;
    public userLoginForm: FormGroup;
    public registrationForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private Alert: NotificationService,
        private userService: UserService,
        private router: Router,
        private route: ActivatedRoute,
        private authService: AuthenticationService,
    ) {
        this.userLoginForm = this.fb.group(StudentLoginComponent.userLoginForm());
        this.registrationForm = this.fb.group(StudentLoginComponent.registrationForm());
    }

    static userLoginForm = function () {
        return {
            email: ['', Validators.compose([Validators.required])],
            password: ['', Validators.compose([Validators.required])]
        }
    };

    static registrationForm = function () {
        return {
            first_name: ['', Validators.compose([Validators.required])],
            last_name: ['', Validators.compose([Validators.required])],
            password: ['', Validators.compose([Validators.required])],
            email: ['', Validators.compose([Validators.required])],
            password_confirmation: ['', Validators.compose([Validators.required])]
        }
    };
    ngOnInit() {

    }
    public signin() {
        this.loading = true;
        this.authService.login(this.userLoginForm.value).subscribe(
            (signinResponse) => {
                // this._router.navigate([this.returnUrl]);
                console.log("User token", signinResponse);
                this.userService.setAuthUser(signinResponse.token);
                //  return (window.location.hostname ==="localhost")? this._router.navigateByUrl('/index') : this._router.navigateByUrl('/american/degree');
                return this.router.navigateByUrl('/portal/student');

            },
            (error) => {
                console.log("There was an error!!");
                this.Alert.error(`Sorry you could not log in`, error);
                this.loading = false;
                return this.router.navigateByUrl('/portal/student'); //remove this when student login is fixed

            });
    }



}
