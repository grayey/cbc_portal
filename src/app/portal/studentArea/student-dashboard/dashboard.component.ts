import { Component, OnInit,AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../../../services/script-loader.service';

@Component({
    selector: 'app-student-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class StudentDashboardComponent implements OnInit,AfterViewInit {

    constructor(
        private _script: ScriptLoaderService,        
    ) { }

    ngOnInit() {
        // alert("Portal student Component!!");
    }

    ngAfterViewInit() {
        this._script.loadScripts('app-student-dashboard',
            [ "./assets/student_assets/app/js/dashboard.js","./assets/student_assets/demo/demo4/base/scripts.bundle.js"],true);
  
    }
}
