import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule,Routes } from "@angular/router";
import { StudentCourseRegistrationComponent } from "./course-registration/course-registration.component";
import { StudentFeesPaymentsComponent } from "./fees-payments/fees-payments.component";
import { StudentProfileComponent } from "./student-profile/student-profile.component";
import { StudentDashboardComponent } from "./dashboard.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";





const STUDENT_DASHBOARD_ROUTES:Routes = [
  
]

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(STUDENT_DASHBOARD_ROUTES)
    ],
    exports: [RouterModule],
    declarations:[
        StudentCourseRegistrationComponent,
        StudentFeesPaymentsComponent,
        StudentProfileComponent,
        StudentDashboardComponent
    ]
})


export class StudentDashboardModule {

}