import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { StudentRoutingModule } from './studentArea/student-routing-module.module';
import { StaffRoutingModule } from './staffArea/staff-routing-module.module';
import { StudentLoginComponent } from './studentArea/student-authentication/student-login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ConfigurationsModule } from './staffArea/staff-dashboard/configurations/configurations.module';

const PORTAL_ROUTES: Routes = [

    { path: "", loadChildren: './studentArea/student-routing-module.module#StudentRoutingModule' },
    { path: "admin", loadChildren: './staffArea/staff-routing-module.module#StaffRoutingModule' },
    
]



@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(PORTAL_ROUTES)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class PortalModule { }
